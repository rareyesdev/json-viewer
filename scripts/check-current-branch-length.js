const { promisify } = require('util');
const exec = promisify(require('child_process').exec);

main();

async function main() {
  const args = process.argv.splice(2);
  const branchLengthLimit = Number(args[0]);

  const branchName = await getCurrentGitBranchName();
  const branchLength = branchName.length;

  if (branchLength > branchLengthLimit) {
    console.error(`Branch ${branchName} has length (${branchLength}). Needs to be <= ${branchLengthLimit}`);
    process.exitCode = 1;
  }
}

async function getCurrentGitBranchName() {
  const { stdout, stderr } = await exec(`git branch | grep \\* | cut -d ' ' -f2`);
  if (stderr) {
    throw new Error(stderr);
  }
  return stdout.replace('\n', '');
}
