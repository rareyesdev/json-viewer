const webpack = require('webpack');
const featureFlags = require('../feature-flags.json');

const nodeEnv = process.env.NODE_ENV;
const finalConfig = Object.assign({}, featureFlags.default, featureFlags[nodeEnv]);

console.log('\nFEATURE FLAGS:');
console.log(JSON.stringify(finalConfig, null, 4), '\n');

const config = new webpack.EnvironmentPlugin(finalConfig);

module.exports = config;
