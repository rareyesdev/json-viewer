const path = require('path');
const autoprefixer = require('autoprefixer');
const cssnano = require('cssnano');
const TerserWebpackPlugin = require('terser-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackExcludeAssetsPlugin = require('html-webpack-exclude-assets-plugin');
const RemoveSourceWebpackPlugin = require('remove-source-webpack-plugin');
const DuplicatePackageCheckerPlugin = require('duplicate-package-checker-webpack-plugin');
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer');
const environmentPluginConfig = require('./environment-plugin-config');

const nodeEnv = process.env.NODE_ENV;
console.log('WEBPACK ENV: ', nodeEnv);
const development = nodeEnv === 'development';
const analyzeBundle = process.env.ANALYZE_BUNDLE;

const minimizeJs = !development;
const minimizeCss = !development;
const extractCss = !development;
const sourceMapCss = !development;
const autoprefixCss = !development;

const enableWebpackBundlerAnalyzer = analyzeBundle ? 'server' : 'disabled';

const semanticUiEntryPointRegex = /custom-semantic-ui__[\d\D]*\.js/;

const MiniCssExtractPluginInstance = new MiniCssExtractPlugin({
  filename: '[name]__[contenthash:7].css',
  chunkFilename: '[name]__[chunkhash:7].css',
});

const postcssLoaderPlugins = [];
minimizeCss && postcssLoaderPlugins.push(cssnano());
autoprefixCss && postcssLoaderPlugins.push(autoprefixer());
const postcssLoader =
  postcssLoaderPlugins.length > 0
    ? [
        {
          loader: 'postcss-loader',
          options: {
            sourceMap: sourceMapCss,
            plugins: () => postcssLoaderPlugins,
          },
        },
      ]
    : [];

const config = {
  mode: nodeEnv,
  entry: {
    bundle: './src/client/index.tsx',
    'custom-semantic-ui': './semantic-ui-theme/semantic.less',
  },
  output: {
    path: path.resolve('dist'),
    publicPath: '/',
  },
  optimization: {
    minimize: minimizeJs,
    minimizer: [new TerserWebpackPlugin()],
    // runtimeChunk: {
    //   name: 'manifest',
    // },
    // splitChunks: {
    //   chunks: 'all',
    // },
  },
  module: {
    rules: [
      // For debugging loaders use inspect-loader like in https://dorp.io/posts/webpack-treeshaking/
      {
        test: /\.(js|ts)x?$/,
        loader: 'babel-loader',
        include: [path.resolve('src/client'), path.resolve('node_modules', 'typesafe-actions')],
      },
      {
        test: /\.less$/,
        use: [
          extractCss ? MiniCssExtractPlugin.loader : 'style-loader',
          {
            loader: 'css-loader',
            options: {
              sourceMap: sourceMapCss,
            },
          },
          ...postcssLoader,
          {
            loader: 'less-loader',
            options: {
              sourceMap: sourceMapCss,
            },
          },
        ],
        include: path.resolve('semantic-ui-theme'),
      },
    ],
  },
  plugins: [
    MiniCssExtractPluginInstance,
    environmentPluginConfig,
    new HtmlWebpackPlugin({
      template: './src/client/index.html',
      filename: 'index.html',
      inject: 'body',
      excludeAssets: [semanticUiEntryPointRegex],
    }),
    new HtmlWebpackExcludeAssetsPlugin(),
    new RemoveSourceWebpackPlugin([semanticUiEntryPointRegex]),
    new CopyWebpackPlugin([{ from: 'static/*', flatten: true }]),
    new BundleAnalyzerPlugin({
      analyzerMode: enableWebpackBundlerAnalyzer,
      // Use https://www.npmjs.com/package/whybundled to analyze stats.json
      generateStatsFile: enableWebpackBundlerAnalyzer === 'server',
    }),
  ],
  resolve: {
    alias: {
      '../../theme.config$': path.resolve('semantic-ui-theme/theme.config'),
      'typesafe-actions': path.resolve('node_modules/typesafe-actions/dist/index.esm.js'),
    },
    extensions: ['.ts', '.tsx', '.js', '.jsx'],
    symlinks: false,
  },
};

if (analyzeBundle) {
  config.plugins.push(new DuplicatePackageCheckerPlugin({ verbose: true, emitError: true }));
}

module.exports = config;
