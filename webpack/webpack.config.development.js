const path = require('path');
const webpack = require('webpack');
const webpackMerge = require('webpack-merge');
// const WriteFilePlugin = require('write-file-webpack-plugin');
const baseConfig = require('./webpack.config.base');

const config = {
  output: {
    filename: '[name].js',
    chunkFilename: '[name].js',
  },
  module: {
    rules: [
      {
        test: /\.(png|svg|jpg|gif|ico|woff|woff2|eot|ttf|otf)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name]__[hash:7].[ext]',
            },
          },
        ],
        include: [path.resolve('src'), path.resolve('node_modules', 'semantic-ui-less')],
      },
    ],
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    // new WriteFilePlugin(),
  ],
  devtool: 'cheap-module-source-map',
  devServer: {
    contentBase: path.resolve('dist'),
    historyApiFallback: true,
    hot: true,
    host: '0.0.0.0',
  },
};

module.exports = webpackMerge(baseConfig, config);
