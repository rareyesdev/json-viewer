export interface JsonEntityBase {
  name: string;
  content: string;
}

export interface JsonEntity extends JsonEntityBase {
  id: string;
}

enum DbMode {
  ReadOnly = 'readonly',
  ReadWrite = 'readwrite',
}

export class IndexDb {
  private databaseName = 'JSON-VIEWER-DB';
  private databaseVersion = 1;
  private fileStoreName = 'files';

  private db?: IDBDatabase;

  public put = async (json: JsonEntityBase) => {
    await this.init();
    const promise = new Promise((resolve, reject) => {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const transaction = this.db!.transaction(this.fileStoreName, DbMode.ReadWrite);
      transaction.oncomplete = function() {
        resolve();
      };
      transaction.onerror = IndexDb.transactionOnErrorHandlerCreator(reject);
      const store = transaction.objectStore(this.fileStoreName);
      store.put(json);
    });
    return promise;
  };

  public remove = async (name: string) => {
    await this.init();
    const promise = new Promise((resolve, reject) => {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const transaction = this.db!.transaction(this.fileStoreName, DbMode.ReadWrite);
      transaction.oncomplete = function() {
        resolve();
      };
      transaction.onerror = IndexDb.transactionOnErrorHandlerCreator(reject);
      const store = transaction.objectStore(this.fileStoreName);
      store.delete(name);
    });
    return promise;
  };

  public get = async (name: string) => {
    await this.init();
    const promise = new Promise<JsonEntity>((resolve, reject) => {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const transaction = this.db!.transaction(this.fileStoreName, DbMode.ReadOnly);
      transaction.onerror = IndexDb.transactionOnErrorHandlerCreator(reject);
      const store = transaction.objectStore(this.fileStoreName);
      const index = store.index('name');
      index.get(name).onsuccess = function() {
        resolve(this.result);
      };
    });
    return promise;
  };

  public getAllNames = async () => {
    await this.init();
    const promise = new Promise((resolve, reject) => {
      // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
      const transaction = this.db!.transaction(this.fileStoreName, DbMode.ReadOnly);
      transaction.onerror = IndexDb.transactionOnErrorHandlerCreator(reject);
      const store = transaction.objectStore(this.fileStoreName);
      store.getAll().onsuccess = function() {
        const names = this.result.map((entity: JsonEntityBase) => entity.name);
        resolve(names);
      };
    });
    return promise;
  };

  private init = async () => {
    if (!this.db) {
      this.db = await this.open();
    }
  };

  private open = async () => {
    const self = this;
    const promise = new Promise<IDBDatabase>((resolve, reject) => {
      const request = window.indexedDB.open(this.databaseName, this.databaseVersion);
      request.onsuccess = function() {
        resolve(this.result);
      };
      request.onerror = function() {
        reject(IndexDb.getEventError(this.error));
      };
      request.onupgradeneeded = function() {
        const store = this.result.createObjectStore(self.fileStoreName, {
          keyPath: 'name',
        });
        store.createIndex('name', 'name', { unique: true });
      };
    });
    return promise;
  };

  private static transactionOnErrorHandlerCreator = (rejectCallback: (error: string) => void) =>
    function onerror(this: IDBTransaction) {
      return rejectCallback(IndexDb.getEventError(this.error));
    };

  private static getEventError(error: DOMException | null) {
    if (error) {
      return error.message;
    }
    return 'Unknown error';
  }
}
