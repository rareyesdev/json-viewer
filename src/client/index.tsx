import createCache from '@emotion/cache';
import { CacheProvider } from '@emotion/core';
import { createBrowserHistory } from 'history';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as ReactGA from 'react-ga';
import { Provider } from 'react-redux';
import { Router } from 'react-router';
import { Redirect, Route, Switch } from 'react-router-dom';
import { applyMiddleware, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';

import { App } from './app';
import { ErrorBoundary } from './error-handling/error-boundary';
import { rootReducer } from './flux/reducers';
import { rootSaga } from './flux/sagas';

//https://github.com/zalmoxisus/redux-devtools-extension#usage
const composeEnhancers = process.env.NODE_ENV === 'development' ? composeWithDevTools : undefined;

const sagaMiddleware = createSagaMiddleware();

const store = createStore(
  rootReducer,
  process.env.NODE_ENV === 'development' && composeEnhancers
    ? composeEnhancers(applyMiddleware(sagaMiddleware))
    : applyMiddleware(sagaMiddleware)
);

sagaMiddleware.run(rootSaga);

// Test that we are not leaking subscriber calls
if (process.env.NODE_ENV === 'development') {
  store.subscribe(() => {
    console.log('CALLING SUBS');
  });
}

// Initialize Google Analytics
ReactGA.initialize('UA-128570758-1', {
  debug: process.env.ENABLE_GA_DEBUG === 'yes',
});
ReactGA.pageview('/');

// Setup GA for page navigation
const browserHistory = createBrowserHistory();
browserHistory.listen(location => ReactGA.pageview(location.pathname));

// Setup emotion cache for setting custom DOM node.
const emotionCache = createCache({ container: document.getElementById('emotion-container') || undefined });

function Wrapper() {
  return (
    <ErrorBoundary>
      <Provider store={store}>
        <CacheProvider value={emotionCache}>
          <Router history={browserHistory}>
            <Switch>
              <Redirect exact from="/" to="/edit" />
              <Route component={App} path="/" />
            </Switch>
          </Router>
        </CacheProvider>
      </Provider>
    </ErrorBoundary>
  );
}

ReactDOM.render(<Wrapper />, document.getElementById('root'));
