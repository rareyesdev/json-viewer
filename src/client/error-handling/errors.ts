import { ExtendableError } from 'ts-error';

export class ApplicationError extends ExtendableError {
  public innerError?: Error;

  public constructor(message: string, innerError?: Error) {
    super(message);
    this.name = this.constructor.name;
    this.innerError = innerError;
  }
}

export class UserFriendlyError extends ApplicationError {
  public constructor(message: string, innerError?: Error) {
    super(message, innerError);
    this.name = this.constructor.name;
  }
}
