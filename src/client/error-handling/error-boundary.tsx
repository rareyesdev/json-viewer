import * as React from 'react';
import * as ReactGA from 'react-ga';

import { ApplicationError, UserFriendlyError } from './errors';

interface ErrorChain {
  message: string;
  innerError?: ErrorChain;
}

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface ErrorBoundaryProps {}

interface ErrorBoundaryState {
  hasError: boolean;
  publicMessageObject?: ErrorChain;
}

export class ErrorBoundary extends React.Component<ErrorBoundaryProps, ErrorBoundaryState> {
  public constructor(props: ErrorBoundaryProps) {
    super(props);
    this.state = { hasError: false };
  }

  public componentDidCatch(error: Error /*, info: any*/) {
    const publicMessageObject = this.buildPublicErrorChain(error as ApplicationError);
    // Display fallback UI
    this.setState({ hasError: true, publicMessageObject });
    // Log the error to a destination distinct from browser console. The error is already there.
    // Track Error using custom event instead of ReactGA.exception cause the last one gives less
    // details in GA dashboard.
    ReactGA.event({
      category: 'Error',
      action: `Caught ${error.name} in Application Error Boundary`,
      label: error.stack,
    });
  }

  public render() {
    const { hasError, publicMessageObject } = this.state;
    if (hasError) {
      // Render custom fallback UI
      return (
        <div>
          <div style={{ textAlign: 'center' }}>
            <h1>Something went wrong</h1>
            <a href="/">Reload</a>
          </div>
          {this.renderPublicErrorChain(publicMessageObject)}
        </div>
      );
    }
    return this.props.children;
  }

  private buildPublicErrorChain(error: ApplicationError): ErrorChain | undefined {
    if (!error) return undefined;
    if (error instanceof UserFriendlyError) {
      return {
        message: error.message,
        innerError: this.buildPublicErrorChain(error.innerError as ApplicationError),
      };
    }
    return this.buildPublicErrorChain(error.innerError as ApplicationError);
  }

  private renderPublicErrorChain(chain?: ErrorChain) {
    if (!chain) return null;
    return (
      <ul>
        <li>{chain.message}</li>
        {this.renderPublicErrorChain(chain.innerError)}
      </ul>
    );
  }
}
