import { ClassNames } from '@emotion/core';
import * as React from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { Tab } from 'semantic-ui-react';

import { panes } from './components/tab-panes/tab-panes';
import { EditorOptions } from './containers/editor-options';
import { NotificationsSection } from './containers/notifications-section';
import { UserFriendlyError /*, ApplicationError*/ } from './error-handling/errors';

// eslint-disable-next-line @typescript-eslint/no-empty-interface
interface AppProps extends RouteComponentProps {}

export class App extends React.Component<AppProps> {
  public render() {
    return (
      <ClassNames>
        {({ css }) => (
          <>
            <Tab
              css={css`
                height: 100%;
              `}
              defaultActiveIndex={this.getTabPanelActiveIndexFromRoute()}
              panes={panes}
              renderActiveOnly={false}
            />
            <EditorOptions
              className={css`
                position: absolute;
                top: 0;
                right: 10px;
                height: 43px;
              `}
            />
            <NotificationsSection
              className={css`
                position: absolute;
                top: 42px;
                right: 0;
                min-width: 310px;
                max-width: 40%;
              `}
            />
          </>
        )}
      </ClassNames>
    );
  }

  private getTabPanelActiveIndexFromRoute = () => {
    const { location } = this.props;
    if (location.pathname === '/view') {
      return 0;
    }
    if (location.pathname === '/edit') {
      return 1;
    }
    throw new UserFriendlyError(`Unknown route ${location.pathname}`);
    // This is just testing multi-error handling.
    // throw new UserFriendlyError('m1', new ApplicationError('m2', new ApplicationError('m3', new UserFriendlyError('m4', new UserFriendlyError('m5')))));
  };
}
