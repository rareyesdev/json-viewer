import { css } from '@emotion/core';
import * as React from 'react';
import { Message } from 'semantic-ui-react';

import { Notification, NotificationType } from '../types/Notification';

interface NotificationsSectionProps {
  className: string;
  notifications: Notification[];
  removeNotification(notification: Notification): void;
}

export class NotificationsSection extends React.Component<NotificationsSectionProps, {}> {
  public constructor(props: NotificationsSectionProps) {
    super(props);
  }

  public render() {
    const { className, notifications, removeNotification } = this.props;
    return (
      <div className={className}>
        {notifications.map((notification, index) => (
          <NotificationComponent key={index} notification={notification} onDismiss={removeNotification} />
        ))}
      </div>
    );
  }
}

interface NotificationProps {
  notification: Notification;
  onDismiss(notification: Notification): void;
}

class NotificationComponent extends React.PureComponent<NotificationProps> {
  public render() {
    const { notification } = this.props;
    return (
      // Need to apply box-shadow manually cause `floatting` used by SemanticUI gets overriden by `error` class.
      // Also added a bit of room for the close button.
      <Message
        css={css`
          padding-right: 2.5em !important;
          box-shadow: 0px 0px 0px 1px rgba(34, 36, 38, 0.22) inset, 0px 2px 4px 0px rgba(34, 36, 38, 0.12),
            0px 2px 10px 0px rgba(34, 36, 38, 0.15) !important;
        `}
        error={notification.type === NotificationType.Error}
        info={notification.type === NotificationType.Info}
        onDismiss={this.onDismiss}
      >
        {notification.message}
      </Message>
    );
  }

  private onDismiss = () => {
    this.props.onDismiss(this.props.notification);
  };
}
