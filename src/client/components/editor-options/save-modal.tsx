import { css } from '@emotion/core';
import * as React from 'react';
import { Button, Form, Icon, Input, Message, Modal, Popup } from 'semantic-ui-react';

import { FileList } from '../../containers/file-list';
import { SaveModalStore } from '../../flux/reducers/save-modal';

interface SaveModalProps extends SaveModalStore {
  isFileListLoading: boolean;
  open: () => void;
  close: () => void;
  onNameChange: (name: string) => void;
  onSave: () => void;
  onConfirmOverwriteYes: () => void;
  onConfirmOverwriteNo: () => void;
}

export const SaveModal: React.FunctionComponent<SaveModalProps> = ({
  fields,
  error,
  isValid,
  isOpen,
  isSaving,
  isConfirmingOverwrite,
  isFileListLoading,
  open,
  close,
  onNameChange,
  onSave,
  onConfirmOverwriteYes,
  onConfirmOverwriteNo,
}) => {
  function onSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    onSave();
  }
  const isSaveButtonDisabled = !isValid || isSaving || isFileListLoading;
  const saveButton = (
    <Button
      color="green"
      css={css`
        align-self: center;
        margin-left: 0.5em !important;
      `}
      disabled={isSaveButtonDisabled}
      onClick={onSave}
    >
      <Icon loading={isSaving} name={isSaving ? 'spinner' : 'checkmark'} /> {isSaving ? 'Saving' : 'Save'}
    </Button>
  );

  return (
    <Modal
      centered={false}
      closeIcon
      open={isOpen}
      trigger={
        <Button onClick={open} onClose={close}>
          Save
        </Button>
      }
    >
      <Modal.Header>Save JSON</Modal.Header>
      <Modal.Content>
        <Form error={!!error} onSubmit={onSubmit}>
          <Form.Field
            css={css`
              display: flex;
            `}
            error={!!fields.name.error}
            inline
          >
            <label
              css={css`
                align-self: center;
              `}
            >
              Name
            </label>
            <Input
              autoFocus
              css={css`
                flex-grow: 1;
              `}
              required
              value={fields.name.value}
              onChange={event => onNameChange(event.currentTarget.value)}
            />
            <Popup open={isConfirmingOverwrite} position="bottom right" trigger={saveButton}>
              <Popup.Header>Name exists. Overwrite?</Popup.Header>
              <Popup.Content>
                <Button color="red" onClick={onConfirmOverwriteNo}>
                  <Icon name="remove" /> No
                </Button>
                <Button color="green" onClick={onConfirmOverwriteYes}>
                  <Icon name="checkmark" /> Yes
                </Button>
              </Popup.Content>
            </Popup>
          </Form.Field>
          <Message content={error} error header="Error" />
        </Form>
        <FileList onFilenameClick={onNameChange} />
      </Modal.Content>
    </Modal>
  );
};
