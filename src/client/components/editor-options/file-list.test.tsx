import * as React from 'react';
import { render, within } from 'react-testing-library';

import { FileList } from './file-list';

describe('file-list component', () => {
  const defaultProps = {
    filenames: ['file1', 'file2', 'file3'],
    isLoading: false,
    load: () => {},
    onFilenameClick: () => {},
    onFilenameRemoveClick: () => {},
  };

  it('should load files when mounting', () => {
    expect.assertions(1);
    const loadMock = jest.fn();
    const props = Object.assign({}, defaultProps, {
      load: loadMock,
    });
    render(<FileList {...props} />);
    expect(loadMock).toHaveBeenCalled();
  });

  it('should render a list of files', () => {
    expect.assertions(1);
    const props = Object.assign({}, defaultProps);
    const { getAllByText } = render(<FileList {...props} />);
    expect(getAllByText(/^file\d$/)).toHaveLength(3);
  });

  it('should render a loading indicator', () => {
    expect.assertions(0);
    const props = Object.assign({}, defaultProps, {
      isLoading: true,
    });
    const { getByText } = render(<FileList {...props} />);
    getByText(/^Loading\.\.\.$/);
  });

  it('should call onFilenameClick', () => {
    expect.assertions(1);
    const filename = 'file1';
    const onFilenameClickMock = jest.fn();
    const props = Object.assign({}, defaultProps, {
      onFilenameClick: onFilenameClickMock,
    });
    const { getByText } = render(<FileList {...props} />);
    const filenameElement = getByText(new RegExp(`^${filename}$`));
    filenameElement.click();
    expect(onFilenameClickMock).toHaveBeenCalledWith(filename);
  });

  it('should call onFilenameRemoveClick', () => {
    expect.assertions(2);
    const filename = 'file2';
    const onFilenameRemoveClickMock = jest.fn();
    const props = Object.assign({}, defaultProps, {
      onFilenameRemoveClick: onFilenameRemoveClickMock,
    });
    const { container } = render(<FileList {...props} />);
    const secondRow = container.querySelector<HTMLTableRowElement>('tr:nth-child(2)');
    expect(secondRow).not.toBeNull();
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const { getByTitle } = within(secondRow!);
    const removeElement = getByTitle(/^Remove$/);
    removeElement.click();
    expect(onFilenameRemoveClickMock).toHaveBeenCalledWith(filename);
  });
});
