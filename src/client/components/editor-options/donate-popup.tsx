import { css } from '@emotion/core';
import * as React from 'react';
import * as ReactGA from 'react-ga';
import { Button, Popup } from 'semantic-ui-react';

export const DonatePopup: React.FunctionComponent = () => {
  if (process.env.ENABLE_DONATION === 'no') return null;
  return (
    <Popup
      hoverable={true}
      on={['hover', 'click']}
      trigger={<Button icon="coffee" title="Buy Me a Coffee (Donate)" />}
      wide
      onOpen={onOpen}
    >
      <Popup.Content>
        <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_blank" onSubmit={onSubmit}>
          <input name="cmd" type="hidden" value="_s-xclick" />
          <input name="hosted_button_id" type="hidden" value="VLQ2TLVWRV6F4" />
          <input name="image_url" type="hidden" value="https://json-viewer.netlify.com/favicon-32x32.png" />
          {/*
          // @ts-ignore */}
          <input
            alt="Donate with PayPal button"
            css={css`
              display: block;
              margin: 0 auto;
              background: url('https://json-viewer.netlify.com/buy-me-a-coffee.png');
              background-position: 0px 0px;
              background-repeat: no-repeat;
              width: 190px;
              height: 39px;
              cursor: pointer;
              border: 0;
            `}
            title="PayPal - The safer, easier way to pay online!"
            type="submit"
            value=""
          />
        </form>
        <div
          css={css`
            font-size: 1.1rem;
          `}
        >
          * Clicking <b>Buy Me a Coffee</b> button will take you to a secure PayPal payment page where you can use
          PayPal or a Credit/Debit card.
        </div>
      </Popup.Content>
    </Popup>
  );
};

function onOpen() {
  ReactGA.modalview('/donate');
}

function onSubmit() {
  /* istanbul ignore next */
  ReactGA.outboundLink({ label: 'PayPal donate page' }, () => {});
}
