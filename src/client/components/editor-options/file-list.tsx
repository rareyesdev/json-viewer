import { css } from '@emotion/core';
import * as React from 'react';
import { Icon, Table } from 'semantic-ui-react';

interface FileListProps {
  filenames: string[];
  isLoading: boolean;
  load: () => void;
  onFilenameClick: (filename: string) => void;
  onFilenameRemoveClick: (filename: string) => void;
}

export class FileList extends React.Component<FileListProps> {
  public componentDidMount() {
    this.props.load();
  }

  public render() {
    const { filenames, isLoading, onFilenameClick } = this.props;
    return (
      <Table compact selectable striped>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Saved Files</Table.HeaderCell>
          </Table.Row>
        </Table.Header>
        <Table.Body>
          {isLoading ? (
            <Table.Row>
              <Table.Cell>
                <Icon loading name="spinner" /> Loading...
              </Table.Cell>
            </Table.Row>
          ) : (
            filenames.map(filename => (
              <Table.Row key={filename} onClick={() => onFilenameClick(filename)}>
                <Table.Cell>
                  <Icon aria-label="File" name="file outline" />
                  <span>{filename}</span>
                  <Icon
                    aria-label="Remove"
                    css={css`
                      float: right;
                      cursor: pointer;
                    `}
                    name="trash alternate outline"
                    title="Remove"
                    onClick={this.onFilenameRemoveClick(filename)}
                  />
                </Table.Cell>
              </Table.Row>
            ))
          )}
        </Table.Body>
      </Table>
    );
  }

  private onFilenameRemoveClick = (filename: string) => (event: React.MouseEvent) => {
    event.stopPropagation();
    this.props.onFilenameRemoveClick(filename);
  };
}
