import { css } from '@emotion/core';
import * as React from 'react';
import { Button, Dropdown, Icon, Input, Responsive } from 'semantic-ui-react';

import { LoadModal } from '../../containers/load-modal';
import { SaveModal } from '../../containers/save-modal';
import { AboutPopup } from './about-popup';
import { DonatePopup } from './donate-popup';

interface FormatButtonProps {
  onClick: React.MouseEventHandler<HTMLButtonElement>;
}

const FormatButton: React.SFC<FormatButtonProps> = ({ onClick }) => (
  <Button compact onClick={onClick}>
    <Icon fitted name="align justify" /> Format
  </Button>
);

interface EditorOptionsProps {
  className: string;
  spaceCount: number;
  isWrapModeOn: boolean;
  formatJson(spaceCount: number): void;
  updateSpaceCount(spaceCount: number): void;
  toggleWrapMode(): void;
}

export class EditorOptions extends React.Component<EditorOptionsProps> {
  public constructor(props: EditorOptionsProps) {
    super(props);
  }

  public render() {
    const { className } = this.props;
    return (
      <React.Fragment>
        <Responsive
          className={className}
          css={css`
            display: flex;
          `}
          minWidth={768}
        >
          {this.renderDefault()}
        </Responsive>
        <Responsive
          className={className}
          css={css`
            display: flex;
          `}
          maxWidth={767}
        >
          {this.renderMenu()}
        </Responsive>
      </React.Fragment>
    );
  }

  private renderDefault() {
    return (
      <div
        css={css`
          margin: auto;
        `}
      >
        {this.renderFormatOption()}
        {this.renderWrapOption()}
        <LoadModal />
        <SaveModal />
        <AboutPopup />
      </div>
    );
  }

  private renderMenu() {
    return (
      <Dropdown
        button
        css={css`
          margin: auto !important;
        `}
        direction="left"
        icon="wrench"
        pointing="top right"
        simple
      >
        <Dropdown.Menu>
          <DropdownItem>{this.renderFormatOption()}</DropdownItem>
          <Dropdown.Divider />
          <DropdownItem>{this.renderWrapOption()}</DropdownItem>
          <Dropdown.Divider />
          <DropdownItem>
            <LoadModal />
          </DropdownItem>
          <DropdownItem>
            <SaveModal />
          </DropdownItem>
          <Dropdown.Divider />
          <DropdownItem>
            <DonatePopup />
            <AboutPopup />
          </DropdownItem>
        </Dropdown.Menu>
      </Dropdown>
    );
  }

  private renderFormatOption() {
    const { spaceCount } = this.props;
    return (
      <Input
        css={css`
          & > input {
            width: 60px;
          }
        `}
        label={<FormatButton onClick={this.formatOnClick} />}
        labelPosition="right"
        min="0"
        size="small"
        step="1"
        type="number"
        value={spaceCount}
        onChange={this.spaceCountOnChange}
      />
    );
  }

  private renderWrapOption() {
    const { isWrapModeOn } = this.props;
    return (
      <Button active={isWrapModeOn} toggle onClick={this.wrapModeOnClick}>
        <Icon fitted name={isWrapModeOn ? 'toggle on' : 'toggle off'} /> Wrap
      </Button>
    );
  }

  private formatOnClick = () => {
    const { spaceCount, formatJson } = this.props;
    formatJson(spaceCount);
  };

  private spaceCountOnChange = (event: React.SyntheticEvent<HTMLInputElement>) => {
    this.props.updateSpaceCount(Number(event.currentTarget.value));
  };

  private wrapModeOnClick = () => {
    this.props.toggleWrapMode();
  };
}

const DropdownItem: React.FunctionComponent = function DropdownItem(props) {
  return (
    <Dropdown.Item
      css={css`
        display: flex !important;
        justify-content: center;
      `}
      {...props}
    />
  );
};
