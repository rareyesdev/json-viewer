import { css } from '@emotion/core';
import * as React from 'react';
import { Button, Modal } from 'semantic-ui-react';

import { FileList } from '../../containers/file-list';
import { LoadModalStore } from '../../flux/reducers/load-modal';

interface LoadModalProps extends LoadModalStore {
  open: () => void;
  close: () => void;
  onLoad: (filename: string) => void;
}

export const LoadModal: React.FunctionComponent<LoadModalProps> = ({ isOpen, isLoading, open, close, onLoad }) => {
  return (
    <Modal
      centered={false}
      closeIcon
      css={css`
        cursor: ${isLoading ? 'wait' : 'inherit'};
      `}
      open={isOpen}
      trigger={<Button onClick={open}>Load</Button>}
      onClose={close}
    >
      <Modal.Header>Load JSON</Modal.Header>
      <Modal.Content>
        <FileList onFilenameClick={onLoad} />
      </Modal.Content>
    </Modal>
  );
};
