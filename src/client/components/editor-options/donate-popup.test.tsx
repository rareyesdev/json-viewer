import * as React from 'react';
import * as ReactGA from 'react-ga';
import { render } from 'react-testing-library';

import { DonatePopup } from './donate-popup';

const getInputElement = () => document.querySelector<HTMLInputElement>('input[alt="Donate with PayPal button');

describe('donate-popup component', () => {
  it('should render', () => {
    expect.assertions(1);
    const { container } = render(<DonatePopup />);
    expect(container).toMatchInlineSnapshot(`
            <div>
              <button
                class="ui icon button"
                role="button"
                title="Buy Me a Coffee (Donate)"
              >
                <i
                  aria-hidden="true"
                  class="coffee icon"
                />
              </button>
            </div>
        `);
  });

  it('should not render', () => {
    expect.assertions(1);
    const enableDonationEnvValue = process.env.ENABLE_DONATION;
    process.env.ENABLE_DONATION = 'no';
    const { container } = render(<DonatePopup />);
    process.env.ENABLE_DONATION = enableDonationEnvValue;
    expect(container).toMatchInlineSnapshot(`<div />`);
  });

  it('should display popup', () => {
    expect.assertions(2);
    const { getByTitle } = render(<DonatePopup />);
    const button = getByTitle('Buy Me a Coffee (Donate)');
    button.click();
    expect(ReactGA.modalview).toHaveBeenCalledWith('/donate');
    const input = getInputElement();
    expect(input).not.toBeNull();
  });

  it('should send form', () => {
    expect.assertions(3);
    const { getByTitle } = render(<DonatePopup />);
    const button = getByTitle('Buy Me a Coffee (Donate)');
    button.click();
    const form = document.querySelector<HTMLFormElement>('form[action="https://www.paypal.com/cgi-bin/webscr"]');
    expect(form).not.toBeNull();
    // Prevent the actual submit event because JSDom doesn't support it
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    const onSubmitEventHandler = form!.onsubmit;
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    form!.onsubmit = function(e) {
      if (onSubmitEventHandler) {
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        onSubmitEventHandler.call(form!, e);
      }
      e.preventDefault();
    };
    const input = getInputElement();
    expect(input).not.toBeNull();
    // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
    input!.click();
    expect(ReactGA.outboundLink).toHaveBeenCalledWith({ label: 'PayPal donate page' }, expect.anything());
  });
});
