import * as React from 'react';
import { Button, Popup } from 'semantic-ui-react';

export const AboutPopup: React.FunctionComponent = () => {
  return (
    <Popup hoverable={true} on={['hover', 'click']} trigger={<Button icon="info" />} wide>
      <Popup.Header>
        About
        <hr />
      </Popup.Header>
      <Popup.Content>
        <p>
          <b>Author: </b>rareyes
        </p>
        <p>
          <b>Icons: </b>
          <a href="https://www.flaticon.com/authors/freepik" rel="noopener noreferrer" target="_blank">
            {' '}
            Freepik
          </a>
          ,
          <a href="https://www.flaticon.com/authors/vaadin" rel="noopener noreferrer" target="_blank">
            {' '}
            Vaadin{' '}
          </a>
          and
          <a href="https://www.flaticon.com/authors/daniel-bruce" rel="noopener noreferrer" target="_blank">
            {' '}
            Daniel Bruce{' '}
          </a>
          from
          <a href="http://www.flaticon.com/" rel="noopener noreferrer" target="_blank">
            {' '}
            Flaticon
          </a>
          ; and
          <a href="https://fontawesome.com/" rel="noopener noreferrer" target="_blank">
            {' '}
            Font Awesome
          </a>
          .
        </p>
      </Popup.Content>
    </Popup>
  );
};
