import { css } from '@emotion/core';
import * as React from 'react';
import { Link, RouteComponentProps, withRouter } from 'react-router-dom';
import { Loader, MenuItemProps, Tab } from 'semantic-ui-react';

// Think about default export vs non-standard module resolution.
// https://github.com/facebook/react/issues/13962
const LazyEditor = React.lazy(() =>
  import(/* webpackChunkName: "editor-lazy-chunk" */ '../../containers/editor').then(module => ({
    default: module.Editor,
  }))
);
const LazyViewer = React.lazy(() =>
  import(/* webpackChunkName: "viewer-lazy-chunk" */ '../../containers/viewer').then(module => ({
    default: module.Viewer,
  }))
);

interface MenuItemRendererProps {
  className?: string;
  onClick: React.MouseEventHandler;
  children?: React.ReactNode;
}

const ViewMenuItem: MenuItemProps = {
  key: 'view',
  icon: 'align left',
  content: 'View',
  as: function MenuItemRenderer({ className, onClick, children }: MenuItemRendererProps) {
    return (
      <Link className={className} to="/view" onClick={onClick}>
        {children}
      </Link>
    );
  },
};

const EditMenuItem: MenuItemProps = {
  key: 'edit',
  icon: 'edit',
  content: 'Edit',
  as: function MenuItemRenderer({ className, onClick, children }: MenuItemRendererProps) {
    return (
      <Link className={className} to="/edit" onClick={onClick}>
        {children}
      </Link>
    );
  },
};

const TabPane: React.FunctionComponent = function TabPane(props) {
  return (
    <Tab.Pane
      css={css`
        height: calc(100% - 43px);
      `}
      {...props}
    />
  );
};

// const TabPane = styled(Tab.Pane)`
//   height: calc(100% - 43px);
// `;

function LoaderContainer() {
  return (
    <div
      css={css`
        height: 100%;
        width: 100%;
      `}
    >
      <Loader active>Loading</Loader>
    </div>
  );
}

interface PaneWrapperProps extends RouteComponentProps {
  initialPaneRoute: string;
  lazy: React.LazyExoticComponent<React.ComponentType>;
}

/* This wrapper is required to make Semantic UI Tab mount each pane only once.
   renderActiveOnly={false} allows to show and hide each pane but we still need to make sure the initially hidden
   pane is not lazy-loaded at the beginning.
   That's why we check the current route before mounting the lazy loaded component.
 */
class PaneWrapper extends React.PureComponent<PaneWrapperProps> {
  private paneRoute = this.props.initialPaneRoute;
  private shouldDynamicallyImport = false;

  public render() {
    const {
      lazy: Lazy,
      location: { pathname },
    } = this.props;
    this.shouldDynamicallyImport = this.shouldDynamicallyImport || pathname === this.paneRoute;
    return <React.Suspense fallback={<LoaderContainer />}>{this.shouldDynamicallyImport && <Lazy />}</React.Suspense>;
  }
}

const PaneWrapperWithRouter = withRouter(PaneWrapper);

export const panes = [
  {
    menuItem: ViewMenuItem,
    pane: (
      <TabPane key={'viewer'}>
        <PaneWrapperWithRouter initialPaneRoute="/view" lazy={LazyViewer} />
      </TabPane>
    ),
  },
  {
    menuItem: EditMenuItem,
    pane: (
      <TabPane key={'editor'}>
        <PaneWrapperWithRouter initialPaneRoute="/edit" lazy={LazyEditor} />
      </TabPane>
    ),
  },
];
