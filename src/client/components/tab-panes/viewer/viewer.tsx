import { css } from '@emotion/core';
import * as React from 'react';

import { BaseNode, ObjectNode } from '../../../types/TreeView';
import { BaseNodeComponent } from './base-node/base-node';

interface ViewerProps {
  treeView?: BaseNode;
  isWrapModeOn: boolean;
  updateCollapsedState(node: ObjectNode): void;
}

export class Viewer extends React.Component<ViewerProps> {
  public render() {
    const { treeView, isWrapModeOn } = this.props;
    // console.log('TV: ', JSON.stringify(treeView));
    return (
      <div
        css={css`
          height: 100%;
          overflow: auto;
          font-family: Arial, sans-serif;
          font-size: 12px;
        `}
      >
        <div
          css={css`
            padding-left: 5px;
            padding-right: 5px;
            width: ${isWrapModeOn ? 'initial' : 'max-content;'};
          `}
        >
          {treeView && <BaseNodeComponent handleCollapseOnClick={this.handleCollapseOnClick} node={treeView} />}
        </div>
      </div>
    );
  }

  private handleCollapseOnClick = (node: ObjectNode) => () => {
    this.props.updateCollapsedState(node);
  };
}
