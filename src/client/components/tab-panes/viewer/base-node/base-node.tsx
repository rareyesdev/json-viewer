import { css } from '@emotion/core';
import * as React from 'react';

import { BaseNode, ObjectNode, ValueNode } from '../../../../types/TreeView';
import { CollapseIcon } from './collapse-icon';
import { FirstLevelPipeConnectorIcon, OuterLevelPipeConnectorIcon } from './pipe-connector-icons';
import { TreeViewIcon } from './tree-view-icon';

interface BaseNodeComponentProps {
  node: BaseNode;
  handleCollapseOnClick(node: ObjectNode): () => void;
}

export class BaseNodeComponent extends React.PureComponent<BaseNodeComponentProps> {
  public render() {
    const { node: root, handleCollapseOnClick } = this.props;
    const type = typeof root;
    let result;
    const pipes = this.renderPipes(root.parentNode);
    if (root instanceof ObjectNode) {
      const children = Array.from(root.children.values()).sort((a, b) => a.propertyIndex - b.propertyIndex);
      result = (
        <React.Fragment>
          <Row onClick={handleCollapseOnClick(root)}>
            {pipes}
            <FixedWidthIconWrapper>
              <CollapseIcon isCollapsed={root.isCollapsed} />
            </FixedWidthIconWrapper>
            <FixedWidthIconWrapper>
              <TreeViewIcon name={root.type} />
            </FixedWidthIconWrapper>
            <span>
              <b>{`${root.propertyName}`}</b>
            </span>
          </Row>
          {!root.isCollapsed &&
            children.map(child => (
              <BaseNodeComponent handleCollapseOnClick={handleCollapseOnClick} key={child.propertyName} node={child} />
            ))}
        </React.Fragment>
      );
    } else if (root instanceof ValueNode) {
      const parentElementCount = root.parentNode ? root.parentNode.children.size : 1;
      result = (
        <Row>
          {pipes}
          <FixedWidthIconWrapper>
            <FirstLevelPipeConnectorIcon currentIndex={root.propertyIndex} elementCount={parentElementCount} />
          </FixedWidthIconWrapper>
          <FixedWidthIconWrapper>
            <TreeViewIcon name={root.type} />
          </FixedWidthIconWrapper>
          <span>
            <b>{`${root.propertyName} : `}</b>
            {root.value}
          </span>
        </Row>
      );
    } else {
      throw new Error('Unknown node of type: ' + type);
    }
    return <React.Fragment>{result}</React.Fragment>;
  }

  private renderPipes(root?: BaseNode): React.ReactNode[] {
    if (root) {
      const pipes = this.renderPipes(root.parentNode);
      const pipe = (
        <FixedWidthIconWrapper key={root.depth}>
          <OuterLevelPipeConnectorIcon
            currentIndex={root.propertyIndex}
            elementCount={root.parentNode ? root.parentNode.children.size : 1}
          />
        </FixedWidthIconWrapper>
      );
      pipes.push(pipe);
      return pipes;
    }
    return [];
  }
}

const Row: React.FunctionComponent<{ onClick?: (event: React.MouseEvent<Element>) => void }> = function Row(props) {
  return (
    <div
      css={css`
        cursor: pointer;
        display: flex;
        &:hover {
          background-color: #e4e4e4;
        }
      `}
      {...props}
    />
  );
};

// const Row = styled('div')`
//   cursor: pointer;
//   display: flex;
//   &:hover {
//     background-color: #e4e4e4;
//   }
// `;

const FixedWidthIconWrapper: React.FunctionComponent = function FixedWidthIconWrapper(props) {
  return (
    <span
      css={css`
        display: inline-block;
        height: auto;
        width: 20px;
        min-width: 20px;

        i.icons,
        && i.icon {
          margin: auto;
          width: 100%;
        }
      `}
      {...props}
    />
  );
};

// const FixedWidthIconWrapper = styled('span')`
//   display: inline-block;
//   height: auto;
//   width: 20px;
//   min-width: 20px;

//   i.icons,
//   && i.icon {
//     margin: auto;
//     width: 100%;
//   }
// `;
