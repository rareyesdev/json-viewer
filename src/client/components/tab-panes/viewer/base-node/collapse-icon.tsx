import * as React from 'react';
import { Icon } from 'semantic-ui-react';

interface CollapseIconProps {
  isCollapsed: boolean;
}

export const CollapseIcon = React.memo<CollapseIconProps>(function CollapseIcon({ isCollapsed }) {
  return (
    <Icon.Group>
      <Icon name="square outline" />
      <Icon name={isCollapsed ? 'plus' : 'minus'} size="small" />
    </Icon.Group>
  );
});
