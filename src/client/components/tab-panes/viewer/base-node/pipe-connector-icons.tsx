import { css } from '@emotion/core';
import * as React from 'react';

interface PipeConnectorIconProps {
  currentIndex: number;
  elementCount: number;
}

export const FirstLevelPipeConnectorIcon = React.memo<PipeConnectorIconProps>(function FirstLevelPipeConnectorIcon({
  currentIndex,
  elementCount,
}) {
  const isLastElement = currentIndex === elementCount - 1;
  return (
    <div
      css={css`
        text-align: center;
      `}
    >
      {isLastElement ? '└' : '├'}
    </div>
  );
});

export const OuterLevelPipeConnectorIcon = React.memo<PipeConnectorIconProps>(function OuterLevelPipeConnectorIcon({
  currentIndex,
  elementCount,
}) {
  const isLastElement = currentIndex === elementCount - 1;
  return (
    <div
      css={css`
        text-align: center;
      `}
    >
      {isLastElement ? '' : '│'}
    </div>
  );
});
