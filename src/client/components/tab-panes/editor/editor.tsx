import { css } from '@emotion/core';
import * as React from 'react';

import { ApplicationError } from '../../../error-handling/errors';
import { ParseError } from '../../../flux/reducers/json/json';
import { DebouncedFunction, debounce } from './debounce';

interface EditorProps {
  json: string;
  parsingError?: ParseError;
  isWrapModeOn: boolean;
  updateJson(newValues: string): void;
}

interface EditorState {
  highlights: string;
}

export class Editor extends React.Component<EditorProps, EditorState> {
  private textareaRef: React.RefObject<HTMLTextAreaElement>;
  private backdropRef: React.RefObject<HTMLDivElement>;
  private setHighlightStateDebounced: DebouncedFunction;

  public state: EditorState = {
    highlights: '',
  };

  public constructor(props: EditorProps) {
    super(props);
    this.textareaRef = React.createRef();
    this.backdropRef = React.createRef();
    this.setHighlightStateDebounced = debounce(this.setHighlightState, 3000);
  }

  public componentDidMount() {
    if (this.textareaRef.current) {
      this.textareaRef.current.focus();
    } else {
      throw new ApplicationError(`Textarea ref is ${this.textareaRef.current}`);
    }
  }

  public componentWillUnmount() {
    this.setHighlightStateDebounced.cleanup();
  }

  public componentDidUpdate() {
    if (this.textareaRef.current) {
      this.updateBackdropScrollPosition(this.textareaRef.current);
    } else {
      throw new ApplicationError(`Textarea ref is ${this.textareaRef.current}`);
    }
  }

  public render() {
    const { json, isWrapModeOn } = this.props;
    let { highlights } = this.state;
    this.setHighlightStateDebounced();
    return (
      <div
        css={css`
          position: relative;
          height: 100%;
          width: 100%;
        `}
      >
        <div
          css={css`
            height: 100%;
            width: 100%;
            overflow: auto;
            position: absolute;
            padding: 2px;
            line-height: 1.15;
            font-family: monospace;
          `}
          ref={this.backdropRef}
        >
          <div
            css={css`
              white-space: ${isWrapModeOn ? 'pre-wrap' : 'pre'};
              word-wrap: break-word;
              color: transparent;
              mark {
                color: transparent;
                background-color: red;
              }
            `}
            dangerouslySetInnerHTML={{ __html: highlights }}
          />
        </div>
        <textarea
          autoFocus={true}
          css={css`
            height: 100%;
            width: 100%;
            border: none;
            outline: none;
            resize: none;
            margin: 0;
            border-radius: 0;
            position: absolute;
            padding: 2px;
            color: #444;
            background-color: transparent;
            font-family: monospace;
            white-space: ${isWrapModeOn ? 'pre-wrap' : 'nowrap'};
          `}
          data-testid="editor-textarea"
          placeholder="Enter JSON here"
          ref={this.textareaRef}
          spellCheck={false}
          value={json}
          onChange={this.handleChange}
          onScroll={this.handleScroll}
        />
      </div>
    );
  }

  private handleChange = (event: React.SyntheticEvent<HTMLTextAreaElement>) => {
    this.props.updateJson(event.currentTarget.value);
  };

  private handleScroll = (event: React.SyntheticEvent<HTMLTextAreaElement>) => {
    this.updateBackdropScrollPosition(event.currentTarget);
  };

  private setHighlightState = () => {
    const { json, parsingError } = this.props;
    // Disable highlights for inouts bugger than 1mb cause layout phase takes too long
    if (json.length < 1024 * 2014) {
      const { highlights: prevHighlights } = this.state;
      const highlights = generateHighlights(json, parsingError);
      if (prevHighlights !== highlights) {
        this.setState({ highlights });
      }
    }
  };

  private updateBackdropScrollPosition(textarea: HTMLTextAreaElement) {
    const scrollTop = textarea.scrollTop;
    const scrollLeft = textarea.scrollLeft;
    if (this.backdropRef.current) {
      this.backdropRef.current.scrollTop = scrollTop;
      this.backdropRef.current.scrollLeft = scrollLeft;
    } else {
      throw new ApplicationError(`Backdrop ref is ${this.backdropRef.current}`);
    }
  }
}

function generateHighlights(json: string, parsingError?: ParseError) {
  let highlights;
  if (parsingError && parsingError.stringIndex != undefined) {
    const offendingCharacter = json[parsingError.stringIndex];
    const additionalWhitespace = /\s/.test(offendingCharacter) ? ' ' : '';
    // if the offending character is an end-of-line character we need to add a dummy whitespace
    // character to be highlighted instead.
    highlights = `${json.substring(0, parsingError.stringIndex)}<mark>${additionalWhitespace}${
      json[parsingError.stringIndex]
    }</mark>${json.substring(parsingError.stringIndex + 1)}`;
  } else {
    highlights = json;
  }
  highlights = highlights.replace(/\n$/g, '\n\n');
  return highlights;
}
