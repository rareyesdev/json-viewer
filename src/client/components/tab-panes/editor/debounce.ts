export interface DebouncedFunction extends Function {
  cleanup: Function;
}

export function debounce(func: Function, delay: number): DebouncedFunction {
  let timerId: NodeJS.Timeout | undefined;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  function debounced(...args: [any]) {
    cleanup();
    timerId = setTimeout(() => {
      func(...args);
      timerId = undefined;
    }, delay);
  }
  function cleanup() {
    if (timerId) {
      clearTimeout(timerId);
    }
  }
  debounced.cleanup = cleanup;
  return debounced;
}
