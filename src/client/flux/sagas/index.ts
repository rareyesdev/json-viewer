import { call, takeEvery, takeLatest } from 'redux-saga/effects';
import { getType } from 'typesafe-actions';

import { load as loadFilenames } from '../actions/filenames';
import { remove as removeJson } from '../actions/filenames';
import { update as updateJson } from '../actions/json';
import { load as loadJson } from '../actions/load-modal';
import { save as saveJson } from '../actions/save-modal';
import { updateSpaceCount as updateSpaceCountSetting } from '../actions/settings';
import { addSavedFileToFilenames } from './add-saved-file-to-filenames';
import { formatJsonWhenSpaceCountIsUpdated } from './format-code-when-space-count-is-updated';
import { loadFilenamesFromDb } from './load-filenames-from-db';
import { loadJsonFromDb } from './load-json-from-db';
import { notifyOnParseErrors } from './notify-on-parse-errors';
import { removeJsonFromDb } from './remove-json-from-db';
import { saveJsonInDb } from './save-json-in-db';

export function* rootSaga() {
  yield takeLatest(getType(loadFilenames), loadFilenamesFromDb);
  yield takeLatest(getType(updateJson), notifyOnParseErrors);
  yield takeLatest(getType(saveJson), saveJsonInDb);
  yield takeLatest(getType(loadJson), loadJsonFromDb);
  yield takeLatest(getType(removeJson), removeJsonFromDb);
  yield takeEvery(getType(updateSpaceCountSetting), formatJsonWhenSpaceCountIsUpdated);
  yield call(addSavedFileToFilenames);
}
