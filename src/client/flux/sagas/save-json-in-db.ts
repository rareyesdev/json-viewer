import { delay } from 'redux-saga';
import { call, put, race, select, take } from 'redux-saga/effects';
import { getType } from 'typesafe-actions';

import { IndexDb, JsonEntityBase } from '../../data-sources/indexdb';
import { NotificationSource, NotificationType } from '../../types/Notification';
import { add as addNotification, remove as removeNotification } from '../actions/notifications';
import {
  confirmOverwriteHide,
  confirmOverwriteNo,
  confirmOverwriteShow,
  confirmOverwriteYes,
  saveComplete,
  saveError,
} from '../actions/save-modal';
import { RootStateType } from '../reducers';

const db = new IndexDb();

export function* saveJsonInDb() {
  const name = yield select((state: RootStateType) => state.saveModal.fields.name.value);
  const content = yield select((state: RootStateType) => state.json.text);
  const filenames: string[] = yield select((state: RootStateType) => state.filenames.filenames);

  let json: JsonEntityBase;
  if (filenames.indexOf(name) !== -1) {
    const confirmed = yield call(confirmOverwrite);
    if (!confirmed) {
      return;
    }
    json = yield call(db.get, name);
    json.content = content;
  } else {
    json = {
      name,
      content,
    };
  }

  try {
    // faking the time it take to save for UI testing purposes
    // yield call(() => new Promise((resolve) => setTimeout(resolve, 1000)));
    yield call(db.put, json);
    yield put(saveComplete(json.name));
    const notification = {
      type: NotificationType.Info,
      source: NotificationSource.SaveModal,
      message: 'Save success',
    };
    yield put(addNotification(notification));
    yield call(delay, 5000);
    yield put(removeNotification(notification));
  } catch (error) {
    yield put(saveError(error.message));
  }
}

function* confirmOverwrite() {
  yield put(confirmOverwriteShow());

  const { yes } = yield race({
    yes: take(getType(confirmOverwriteYes)),
    no: take(getType(confirmOverwriteNo)),
  });

  yield put(confirmOverwriteHide());

  return !!yes;
}
