import { call, put } from 'redux-saga/effects';
import { ActionType } from 'typesafe-actions';

import { IndexDb } from '../../data-sources/indexdb';
import { remove, removeComplete, removeError } from '../actions/filenames';

const db = new IndexDb();

export function* removeJsonFromDb(action: ActionType<typeof remove>) {
  try {
    const filename = action.payload;
    yield call(db.remove, filename);
    yield put(removeComplete(filename));
  } catch (error) {
    yield put(removeError(error.message));
  }
}
