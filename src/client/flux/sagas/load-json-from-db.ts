import { delay } from 'redux-saga';
import { call, put, select } from 'redux-saga/effects';

import { IndexDb } from '../../data-sources/indexdb';
import { NotificationSource, NotificationType } from '../../types/Notification';
import { update as updateJson } from '../actions/json';
import { loadComplete, loadError } from '../actions/load-modal';
import { add as addNotification, remove as removeNotification } from '../actions/notifications';
import { RootStateType } from '../reducers';

const db = new IndexDb();

export function* loadJsonFromDb() {
  const filename = yield select((state: RootStateType) => state.loadModal.filename);

  try {
    // faking the time it take to save for UI testing purposes
    // yield call(() => new Promise((resolve) => setTimeout(resolve, 1000)));
    const file = yield call(db.get, filename);
    yield put(updateJson(file.content));
    yield put(loadComplete());
    const notification = {
      type: NotificationType.Info,
      source: NotificationSource.LoadModal,
      message: 'Load success',
    };
    yield put(addNotification(notification));
    yield call(delay, 5000);
    yield put(removeNotification(notification));
  } catch (error) {
    yield put(loadError(error.message));
  }
}
