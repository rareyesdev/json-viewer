import { put, select } from 'redux-saga/effects';

import { NotificationSource, NotificationType } from '../../types/Notification';
import { Notification } from '../../types/Notification';
import { add as addNotification, remove as removeNotification } from '../actions/notifications';
import { RootStateType } from '../reducers';

export function* notifyOnParseErrors() {
  const parsingError = yield select((state: RootStateType) => state.json.parsingError);
  const notifications: Notification[] = yield select((state: RootStateType) => state.notifications);
  const existingNotification = notifications.find(notification => notification.source === NotificationSource.Parse);
  if (existingNotification) {
    yield put(removeNotification(existingNotification));
  }
  if (parsingError) {
    yield put(
      addNotification({
        type: NotificationType.Error,
        source: NotificationSource.Parse,
        message: parsingError.message,
      })
    );
  }
}
