import { put, select, take } from 'redux-saga/effects';
import { getType } from 'typesafe-actions';

import { add as addFilename } from '../actions/filenames';
import { saveComplete } from '../actions/save-modal';
import { RootStateType } from '../reducers';

export function* addSavedFileToFilenames() {
  while (true) {
    const { payload: name } = yield take(getType(saveComplete));
    const filenames = yield select((state: RootStateType) => state.filenames.filenames);
    if (filenames.indexOf(name) === -1) {
      yield put(addFilename(name));
    }
  }
}
