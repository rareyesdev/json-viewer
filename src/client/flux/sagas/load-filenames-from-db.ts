import { call, put, select } from 'redux-saga/effects';

import { IndexDb } from '../../data-sources/indexdb';
import { loadComplete, loadError } from '../actions/filenames';
import { RootStateType } from '../reducers';

const db = new IndexDb();

export function* loadFilenamesFromDb() {
  const isLoaded = yield select((state: RootStateType) => state.filenames.isLoaded);
  if (!isLoaded) {
    try {
      // faking the time it takes to load for UI testing purposes
      // yield call(() => new Promise((resolve) => setTimeout(resolve, 1000)));
      const names = yield call(db.getAllNames);
      yield put(loadComplete(names));
    } catch (error) {
      yield put(loadError(error.message));
    }
  }
}
