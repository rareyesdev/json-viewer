import { put, select } from 'redux-saga/effects';

import { format as jsonFormatAction } from '../actions/json';
import { RootStateType } from '../reducers';

export function* formatJsonWhenSpaceCountIsUpdated() {
  const count = yield select((state: RootStateType) => state.settings.formatSpaceCount);
  yield put(jsonFormatAction(count));
}
