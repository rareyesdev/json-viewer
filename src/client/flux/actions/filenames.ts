import { createStandardAction } from 'typesafe-actions';

export const load = createStandardAction('filenames/LOAD')();

export const loadComplete = createStandardAction('filenames/LOAD_COMPLETE')<string[]>();

export const loadError = createStandardAction('filenames/LOAD_ERROR')<string>();

export const add = createStandardAction('filenames/ADD')<string>();

export const remove = createStandardAction('filenames/REMOVE')<string>();

export const removeComplete = createStandardAction('filenames/REMOVE_COMPLETE')<string>();

export const removeError = createStandardAction('filenames/REMOVE_ERROR')<string>();
