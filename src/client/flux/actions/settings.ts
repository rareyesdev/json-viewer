import { createStandardAction } from 'typesafe-actions';

export const updateSpaceCount = createStandardAction('settings/UPDATE_SPACE_COUNT')<number>();

export const toggleWrapMode = createStandardAction('settings/TOGGLE_WRAP_MODE')();
