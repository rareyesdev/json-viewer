import { createStandardAction } from 'typesafe-actions';

export const open = createStandardAction('save-modal/OPEN')();

export const close = createStandardAction('save-modal/CLOSE')();

export const update = createStandardAction('save-modal/UPDATE')<string>();

export const save = createStandardAction('save-modal/SAVE')();

export const saveComplete = createStandardAction('save-modal/SAVE_COMPLETE')<string>();

export const saveError = createStandardAction('save-modal/SAVE_ERROR')<string>();

export const confirmOverwriteShow = createStandardAction('save-modal/CONFIRM_OVERWRITE_SHOW')();

export const confirmOverwriteHide = createStandardAction('save-modal/CONFIRM_OVERWRITE_HIDE')();

export const confirmOverwriteYes = createStandardAction('save-modal/CONFIRM_OVERWRITE_YES')();

export const confirmOverwriteNo = createStandardAction('save-modal/CONFIRM_OVERWRITE_NO')();
