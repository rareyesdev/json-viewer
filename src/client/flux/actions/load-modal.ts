import { createStandardAction } from 'typesafe-actions';

export const open = createStandardAction('load-modal/OPEN')();

export const close = createStandardAction('load-modal/CLOSE')();

export const load = createStandardAction('load-modal/LOAD')<string>();

export const loadComplete = createStandardAction('load-modal/LOAD_COMPLETE')();

export const loadError = createStandardAction('load-modal/LOAD_ERROR')<string>();
