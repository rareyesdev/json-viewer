import { createStandardAction } from 'typesafe-actions';

import { Notification } from '../../types/Notification';

export const add = createStandardAction('notifications/ADD')<Notification>();

export const remove = createStandardAction('notifications/REMOVE')<Notification>();
