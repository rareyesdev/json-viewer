import { createStandardAction } from 'typesafe-actions';

import { ObjectNode } from '../../types/TreeView';

export const update = createStandardAction('json/UPDATE')<string>();

export const format = createStandardAction('json/FORMAT')<number>();

export const updateCollapsedState = createStandardAction('json/UPDATE_COLLAPSED_STATE')<ObjectNode>();
