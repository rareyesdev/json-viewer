import { ActionType } from 'typesafe-actions';

import * as filenamesActions from './filenames';
import * as jsonActions from './json';
import * as loadModalActions from './load-modal';
import * as notificationsActions from './notifications';
import * as saveModalActions from './save-modal';
import * as settingsActions from './settings';

export type JsonActionsTypes = ActionType<typeof jsonActions>;

export type NotificationsActionTypes = ActionType<typeof notificationsActions>;

export type FilenamesActionTypes = ActionType<typeof filenamesActions>;

export type SettingsActionTypes = ActionType<typeof settingsActions>;

export type SaveModalActionTypes = ActionType<typeof saveModalActions>;

export type LoadModalActionTypes = ActionType<typeof loadModalActions>;

export type AllActionsTypes =
  | JsonActionsTypes
  | NotificationsActionTypes
  | FilenamesActionTypes
  | SaveModalActionTypes
  | LoadModalActionTypes
  | SettingsActionTypes;
