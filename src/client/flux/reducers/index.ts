import { combineReducers } from 'redux';
import { StateType } from 'typesafe-actions';

import { filenamesReducer } from './filenames';
import { jsonReducer } from './json/json';
import { loadModalReducer } from './load-modal';
import { notificationsReducer } from './notifications';
import { saveModalReducer } from './save-modal';
import { settingsReducer } from './settings';

export const rootReducer = combineReducers({
  json: jsonReducer,
  notifications: notificationsReducer,
  filenames: filenamesReducer,
  settings: settingsReducer,
  saveModal: saveModalReducer,
  loadModal: loadModalReducer,
});

export type RootStateType = StateType<typeof rootReducer>;
