import { StateType, getType } from 'typesafe-actions';

import { Notification } from '../../types/Notification';
import { NotificationsActionTypes } from '../actions/ActionTypes';
import * as actions from '../actions/notifications';

const defaultState: Notification[] = [];

export const notificationsReducer = (state = defaultState, action: NotificationsActionTypes) => {
  switch (action.type) {
    case getType(actions.add):
      return [action.payload].concat(state);
    case getType(actions.remove):
      const index = state.indexOf(action.payload);
      return index === -1 ? state : [...state.slice(0, index), ...state.slice(index + 1)];
    default:
      return state;
  }
};

export type NotificationsStateType = StateType<typeof notificationsReducer>;
