import { getType } from 'typesafe-actions';

import { FilenamesActionTypes } from '../actions/ActionTypes';
import * as actions from '../actions/filenames';

const defaultState: FilenamesStore = {
  filenames: [],
  isLoading: false,
  isLoaded: false,
  error: undefined,
};

export const filenamesReducer = (state = defaultState, action: FilenamesActionTypes) => {
  switch (action.type) {
    case getType(actions.load):
      return state.isLoaded
        ? state
        : {
            ...state,
            isLoading: true,
          };
    case getType(actions.loadComplete):
      return {
        filenames: action.payload,
        isLoading: false,
        isLoaded: true,
      };
    case getType(actions.loadError):
      return {
        filenames: [],
        isLoading: false,
        isLoaded: false,
        error: action.payload,
      };
    case getType(actions.add):
      return {
        ...state,
        filenames: [action.payload].concat(state.filenames),
      };
    case getType(actions.removeComplete):
      // The file was removed from the DB by the sagas, now remove it from the store.
      const index = state.filenames.indexOf(action.payload);
      return index === -1
        ? state
        : {
            ...state,
            filenames: [...state.filenames.slice(0, index), ...state.filenames.slice(index + 1)],
          };
    case getType(actions.removeError):
      return {
        ...state,
        error: action.payload,
      };
    default:
      return state;
  }
};

export interface FilenamesStore {
  filenames: string[];
  isLoading: boolean;
  isLoaded: boolean;
  error?: string;
}
