export const defaultJsonForDebugging = `
{
  "numbers": {
    "number": 1,
    "bigNumber": 123456789,
    "decimal": 1.23456789
  },
  "booleans": {
    "trueValue": true,
    "falseValue": false
  },
  "null value": null,
  "arrays": {
    "array": [],
    "with some elements": [1, 2, 3],
    "with objects inside": [1, { "object-in-array": true }, null, "string in array"]
  },
  "objects": {
    "object": {},
    "with complex key": {},
    "nested object": {
      "this is nested": true
    }
  },
  "strings": {
    "string": "string value",
    "2 lines string": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas tristique odio non gravida commodo. Donec facilisis, magna ut tempor pellentesque, justo lectus pretium nisl, vitae aliquet odio quam eu erat. Fusce ut tempor eros. Fusce sit amet enim ut quam pellentesque viverra non sed elit. Mauris ut felis eget felis.",
    "3 lines string": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed sit amet leo nec tortor consectetur malesuada. Phasellus porta dolor nunc, ac aliquam massa sodales id. In dignissim egestas porttitor. Morbi porttitor odio purus, quis finibus lorem eleifend sed. Nunc euismod diam ut dui bibendum condimentum. Maecenas in erat a dui sollicitudin pulvinar a eu lacus. Fusce efficitur et tortor interdum efficitur. Nullam auctor porta nunc, quis viverra augue auctor sed."
  }
}
`;
