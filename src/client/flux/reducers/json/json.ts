import { StateType, getType } from 'typesafe-actions';

import { BaseNode, ObjectNode, ValueNode } from '../../../types/TreeView';
import { JsonActionsTypes } from '../../actions/ActionTypes';
import * as actions from '../../actions/json';

let defaultState: DefaultState = {
  text: '',
  object: undefined,
  parsingError: undefined,
  treeView: undefined,
  treeViewVersion: 0,
};

export const jsonReducer = (state = defaultState, action: JsonActionsTypes) => {
  switch (action.type) {
    case getType(actions.update): {
      const parseResponse = parseJson(action.payload);
      const treeViewVersion = state.treeViewVersion + 1;
      const treeView = parseResponse.parsingError
        ? state.treeView
        : updateTreeView(parseResponse.object, treeViewVersion, state.treeView);
      return {
        ...state,
        ...parseResponse,
        treeView,
        treeViewVersion,
        text: action.payload,
      };
    }
    case getType(actions.format):
      return {
        ...state,
        text: JSON.stringify(state.object, undefined, action.payload),
      };
    case getType(actions.updateCollapsedState): {
      const treeView = updateCollapsedState(action.payload);
      return {
        ...state,
        treeView,
      };
    }
    default:
      return state;
  }
};

function parseJson(json: string): ParseResponse {
  let object: object | undefined;
  let parsingError;
  try {
    object = JSON.parse(json);
  } catch (error) {
    // TODO: Highlight only works in chrome right now because JSON.parse is returning the error in
    // different formats depending on the browser. Chrome, safari and firefox all have different
    // error messages. Need to find a way to standarize the line and column of the error
    const message: string = error.message;
    const lastWhitespace: number = message.lastIndexOf(' ');
    const stringIndex = Number(message.substring(lastWhitespace + 1));
    parsingError = {
      message,
      stringIndex: isNaN(stringIndex) ? undefined : stringIndex,
    };
  }
  return {
    object,
    parsingError,
  };
}

function updateTreeView(
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  currentProperty: any,
  treeViewVersion: number,
  prevTreeView?: BaseNode,
  currentPropertyName = 'JSON',
  currentPropertyPath = '',
  currentPropertyIndex = 0,
  depth: number = 0
): BaseNode {
  let result = prevTreeView;
  const nodeType = typeof currentProperty;

  if (currentProperty === null) {
    // null
    if (!ValueNode.isInstance(result) || result.type !== 'null') {
      result = new ValueNode(
        currentPropertyName,
        currentPropertyPath,
        currentPropertyIndex,
        depth,
        treeViewVersion,
        'null',
        'null'
      );
    } else {
      result.nodeVersion = treeViewVersion;
      result.propertyIndex = currentPropertyIndex;
    }
    return result;
  }

  if (nodeType === 'object') {
    // object, array
    const isArray = Array.isArray(currentProperty);
    let isNewNode = false;
    let isDirty = false;
    if (!ObjectNode.isInstance(result)) {
      result = new ObjectNode(
        currentPropertyName,
        currentPropertyPath,
        currentPropertyIndex,
        depth,
        treeViewVersion,
        'object'
      );
      isNewNode = true;
    }
    if (isArray) {
      (result as ObjectNode).type = 'array';
      isDirty = true;
    }
    result.nodeVersion = treeViewVersion;
    if (result.propertyIndex !== currentPropertyIndex) {
      result.propertyIndex = currentPropertyIndex;
      isDirty = true;
    }
    const children = (result as ObjectNode).children;
    Object.keys(currentProperty).forEach((propertyName, propertyIndex) => {
      const oldChild = children.get(propertyName);
      const newChild = updateTreeView(
        currentProperty[propertyName],
        treeViewVersion,
        oldChild,
        propertyName,
        `${currentPropertyPath}.${propertyName}`,
        propertyIndex,
        depth + 1
      );
      if (newChild !== oldChild) {
        (result as ObjectNode).addChild(newChild);
        isDirty = true;
      }
    });
    for (const [key, value] of children) {
      if (value.nodeVersion !== treeViewVersion) {
        children.delete(key);
        isDirty = true;
      }
    }
    if (isNewNode || !isDirty) {
      return result;
    } else {
      return result.clone();
    }
  }

  if (nodeType === 'string' || nodeType === 'number' || nodeType === 'boolean') {
    // string, number, boolean
    const currentValue = currentProperty.toString();
    if (ValueNode.isInstance(result)) {
      if (result.value !== currentValue) {
        result = new ValueNode(
          result.propertyName,
          result.propertyPath,
          result.propertyIndex,
          depth,
          treeViewVersion,
          nodeType,
          currentValue
        );
      } else {
        result.nodeVersion = treeViewVersion;
        result.propertyIndex = currentPropertyIndex;
      }
    } else {
      result = new ValueNode(
        currentPropertyName,
        currentPropertyPath,
        currentPropertyIndex,
        depth,
        treeViewVersion,
        nodeType,
        currentValue
      );
    }
    return result;
  }

  throw new Error('Unknown node of type: ' + nodeType);
}

function updateCollapsedState(node: ObjectNode): BaseNode {
  let newNode = node.clone();
  newNode.isCollapsed = !newNode.isCollapsed;
  while (newNode.parentNode) {
    newNode.parentNode.children.set(newNode.propertyName, newNode);
    newNode = newNode.parentNode.clone();
  }
  return newNode;
}

if (process.env.START_WITH_DEBUG_JSON === 'yes') {
  // eslint-disable-next-line @typescript-eslint/no-var-requires
  const { defaultJsonForDebugging } = require('./default-json-for-debugging');
  const defaultParseResponse = parseJson(defaultJsonForDebugging);
  const defaultTreeViewVersion = 0;
  const defaultTreeView = updateTreeView(defaultParseResponse.object, defaultTreeViewVersion, undefined);

  defaultState = {
    ...defaultParseResponse,
    text: defaultJsonForDebugging,
    treeView: defaultTreeView,
    treeViewVersion: defaultTreeViewVersion,
  };
}

export type JsonStateType = StateType<typeof jsonReducer>;

interface DefaultState {
  text: string;
  object?: object;
  parsingError?: ParseError;
  treeView?: BaseNode;
  treeViewVersion: number;
}

export interface ParseError {
  message: string;
  stringIndex?: number;
}

interface ParseResponse {
  object?: object;
  parsingError?: ParseError;
}
