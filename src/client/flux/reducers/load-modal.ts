import { getType } from 'typesafe-actions';

import { LoadModalActionTypes } from '../actions/ActionTypes';
import * as actions from '../actions/load-modal';

const defaultState: LoadModalStore = {
  filename: '',
  isOpen: false,
  isLoading: false,
};

export const loadModalReducer = (state = defaultState, action: LoadModalActionTypes) => {
  switch (action.type) {
    case getType(actions.open):
      return {
        ...state,
        isOpen: true,
      };
    case getType(actions.close):
      return {
        ...state,
        isOpen: false,
      };
    case getType(actions.load):
      return {
        ...state,
        filename: action.payload,
        isLoading: true,
      };
    case getType(actions.loadComplete):
      return {
        ...state,
        isLoading: false,
        isOpen: false,
      };
    case getType(actions.loadError):
      return {
        ...state,
        isLoading: false,
        isOpen: true,
        error: action.payload,
      };
    default:
      return state;
  }
};

export interface LoadModalStore {
  filename: string;
  isOpen: boolean;
  isLoading: boolean;
  error?: string;
}
