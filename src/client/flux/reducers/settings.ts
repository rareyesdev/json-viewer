import { StateType, getType } from 'typesafe-actions';

import { SettingsActionTypes } from '../actions/ActionTypes';
import * as actions from '../actions/settings';

const defaultState = {
  formatSpaceCount: 4,
  isWrapModeOn: true,
};

export const settingsReducer = (state = defaultState, action: SettingsActionTypes) => {
  switch (action.type) {
    case getType(actions.updateSpaceCount):
      return {
        ...state,
        formatSpaceCount: action.payload,
      };
    case getType(actions.toggleWrapMode):
      return {
        ...state,
        isWrapModeOn: !state.isWrapModeOn,
      };
    default:
      return state;
  }
};

export type SettingsStateType = StateType<typeof settingsReducer>;
