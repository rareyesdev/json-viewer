import { getType } from 'typesafe-actions';

import { SaveModalActionTypes } from '../actions/ActionTypes';
import * as actions from '../actions/save-modal';

const defaultState: SaveModalStore = {
  fields: {
    name: {
      value: 'Unnamed file',
      error: undefined,
    },
  },
  error: undefined,
  isValid: true,
  isOpen: false,
  isSaving: false,
  isConfirmingOverwrite: false,
};

export const saveModalReducer = (state = defaultState, action: SaveModalActionTypes) => {
  switch (action.type) {
    case getType(actions.open):
      return {
        ...state,
        isOpen: true,
      };
    case getType(actions.close):
      return {
        ...state,
        isOpen: false,
      };
    case getType(actions.update):
      const name = action.payload;
      const nameValidationResult = validateName(name);
      return {
        ...state,
        fields: {
          name: {
            value: name,
            error: nameValidationResult,
          },
        },
        isValid: !nameValidationResult,
        error: undefined,
      };
    case getType(actions.save):
      return {
        ...state,
        isSaving: true,
      };
    case getType(actions.saveComplete):
      return {
        ...state,
        isSaving: false,
        isOpen: false,
      };
    case getType(actions.saveError):
      return {
        ...state,
        isSaving: false,
        isOpen: true,
        error: action.payload,
      };
    case getType(actions.confirmOverwriteShow):
      return {
        ...state,
        isConfirmingOverwrite: true,
      };
    case getType(actions.confirmOverwriteHide):
      return {
        ...state,
        isConfirmingOverwrite: false,
      };
    case getType(actions.confirmOverwriteNo):
      return {
        ...state,
        isSaving: false,
      };
    default:
      return state;
  }
};

function validateName(value: string) {
  if (!value || !value.length) {
    return 'Name is required';
  }
  return undefined;
}

export interface SaveModalStore {
  fields: {
    name: FormField<string>;
  };
  error?: string;
  isValid: boolean;
  isOpen: boolean;
  isSaving: boolean;
  isConfirmingOverwrite: boolean;
}

interface FormField<TValue> {
  value: TValue;
  error?: string;
}
