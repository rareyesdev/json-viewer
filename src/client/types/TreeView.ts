export class BaseNode {
  public propertyName: string;
  public propertyPath: string;
  public propertyIndex: number;
  public depth: number;
  public nodeVersion: number;
  public parentNode?: ObjectNode;

  public constructor(
    propertyName: string,
    propertyPath: string,
    propertyIndex: number,
    depth: number,
    nodeVersion: number,
    parentNode?: ObjectNode
  ) {
    this.propertyName = propertyName;
    this.propertyPath = propertyPath;
    this.propertyIndex = propertyIndex;
    this.depth = depth;
    this.nodeVersion = nodeVersion;
    this.parentNode = parentNode;
  }

  public clone() {
    const instance = new BaseNode(
      this.propertyName,
      this.propertyPath,
      this.propertyIndex,
      this.depth,
      this.nodeVersion
    );
    return instance;
  }
}

export type ObjectNodeType = 'object' | 'array';

export class ObjectNode extends BaseNode {
  public type: ObjectNodeType;
  public children: Map<string, BaseNode>;
  public isCollapsed: boolean;

  public constructor(
    propertyName: string,
    propertyPath: string,
    propertyIndex: number,
    depth: number,
    nodeVersion: number,
    type: ObjectNodeType,
    parentNode?: ObjectNode
  ) {
    super(propertyName, propertyPath, propertyIndex, depth, nodeVersion, parentNode);
    this.type = type;
    this.children = new Map<string, BaseNode>();
    this.isCollapsed = true;
    if (process.env.EXPAND_NODES_BY_DEFAULT === 'yes') {
      this.isCollapsed = false;
    }
  }

  public addChild(child: BaseNode) {
    child.parentNode = this;
    this.children.set(child.propertyName, child);
  }

  public clone() {
    const instance = new ObjectNode(
      this.propertyName,
      this.propertyPath,
      this.propertyIndex,
      this.depth,
      this.nodeVersion,
      this.type,
      this.parentNode
    );
    instance.children = this.children;
    instance.isCollapsed = this.isCollapsed;
    instance.children.forEach(child => (child.parentNode = instance));
    return instance;
  }

  public static isInstance(instance?: BaseNode): instance is ObjectNode {
    return instance != undefined && instance instanceof ObjectNode;
  }
}

export type ValueNodeType = 'string' | 'number' | 'boolean' | 'null';

export class ValueNode extends BaseNode {
  public type: ValueNodeType;
  public value: string;

  public constructor(
    propertyName: string,
    propertyPath: string,
    propertyIndex: number,
    depth: number,
    nodeVersion: number,
    type: ValueNodeType,
    value: string
  ) {
    super(propertyName, propertyPath, propertyIndex, depth, nodeVersion);
    this.type = type;
    this.value = value;
  }

  public static isInstance(instance?: BaseNode): instance is ValueNode {
    return instance != undefined && instance instanceof ValueNode;
  }
}
