export enum NotificationType {
  Info,
  Warning,
  Error,
}

export enum NotificationSource {
  Parse,
  SaveModal,
  LoadModal,
}

export interface Notification {
  type: NotificationType;
  source: NotificationSource;
  message: string;
}
