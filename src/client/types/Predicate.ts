export interface Predicate<T> {
  (value: T): boolean;
}
