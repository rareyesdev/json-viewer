import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import { LoadModal as LoadModalComponent } from '../components/editor-options/load-modal';
import { LoadModalActionTypes } from '../flux/actions/ActionTypes';
import * as LoadModalActions from '../flux/actions/load-modal';
import { RootStateType } from '../flux/reducers';

const mapStateToProps = (state: RootStateType) => ({
  filename: state.loadModal.filename,
  error: state.loadModal.error,
  isOpen: state.loadModal.isOpen,
  isLoading: state.loadModal.isLoading,
});

const mapDispatchToProps = (dispatch: Dispatch<LoadModalActionTypes>) => ({
  open: () => dispatch(LoadModalActions.open()),
  close: () => dispatch(LoadModalActions.close()),
  onLoad: (filename: string) => dispatch(LoadModalActions.load(filename)),
});

export const LoadModal = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoadModalComponent);
