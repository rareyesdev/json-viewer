import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import { SaveModal as SaveModalComponent } from '../components/editor-options/save-modal';
import { SaveModalActionTypes } from '../flux/actions/ActionTypes';
import * as saveModalActions from '../flux/actions/save-modal';
import { RootStateType } from '../flux/reducers';

const mapStateToProps = (state: RootStateType) => ({
  fields: state.saveModal.fields,
  error: state.saveModal.error,
  isValid: state.saveModal.isValid,
  isOpen: state.saveModal.isOpen,
  isSaving: state.saveModal.isSaving,
  isConfirmingOverwrite: state.saveModal.isConfirmingOverwrite,
  isFileListLoading: state.filenames.isLoading,
});

const mapDispatchToProps = (dispatch: Dispatch<SaveModalActionTypes>) => ({
  open: () => dispatch(saveModalActions.open()),
  close: () => dispatch(saveModalActions.close()),
  onNameChange: (name: string) => dispatch(saveModalActions.update(name)),
  onSave: () => dispatch(saveModalActions.save()),
  onConfirmOverwriteYes: () => dispatch(saveModalActions.confirmOverwriteYes()),
  onConfirmOverwriteNo: () => dispatch(saveModalActions.confirmOverwriteNo()),
});

export const SaveModal = connect(
  mapStateToProps,
  mapDispatchToProps
)(SaveModalComponent);
