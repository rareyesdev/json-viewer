import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import { Viewer as ViewerComponent } from '../components/tab-panes/viewer/viewer';
import { JsonActionsTypes } from '../flux/actions/ActionTypes';
import * as jsonActions from '../flux/actions/json';
import { RootStateType } from '../flux/reducers';
import { ObjectNode } from '../types/TreeView';

const mapStateToProps = (state: RootStateType) => ({
  json: state.json.object,
  treeView: state.json.treeView,
  isWrapModeOn: state.settings.isWrapModeOn,
});

const mapDispatchToProps = (dispatch: Dispatch<JsonActionsTypes>) => ({
  updateCollapsedState: (node: ObjectNode) => dispatch(jsonActions.updateCollapsedState(node)),
});

export const Viewer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ViewerComponent);
