import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import { FileList as FileListComponent } from '../components/editor-options/file-list';
import { FilenamesActionTypes } from '../flux/actions/ActionTypes';
import { load as loadFilenames, remove as removeFilename } from '../flux/actions/filenames';
import { RootStateType } from '../flux/reducers';

const mapStateToProps = (state: RootStateType) => ({
  filenames: state.filenames.filenames,
  error: state.filenames.error,
  isLoading: state.filenames.isLoading,
});

const mapDispatchToProps = (dispatch: Dispatch<FilenamesActionTypes>) => ({
  load: () => dispatch(loadFilenames()),
  onFilenameRemoveClick: (filename: string) => dispatch(removeFilename(filename)),
});

export const FileList = connect(
  mapStateToProps,
  mapDispatchToProps
)(FileListComponent);
