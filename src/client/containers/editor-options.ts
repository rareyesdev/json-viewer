import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import { EditorOptions as EditorOptionsComponent } from '../components/editor-options/editor-options';
import { AllActionsTypes } from '../flux/actions/ActionTypes';
import * as jsonActions from '../flux/actions/json';
import * as settingsActions from '../flux/actions/settings';
import { RootStateType } from '../flux/reducers';

const mapStateToProps = (state: RootStateType) => ({
  spaceCount: state.settings.formatSpaceCount,
  isWrapModeOn: state.settings.isWrapModeOn,
});

const mapDispatchToProps = (dispatch: Dispatch<AllActionsTypes>) => ({
  formatJson: (spaceCount: number) => dispatch(jsonActions.format(spaceCount)),
  updateSpaceCount: (spaceCount: number) => dispatch(settingsActions.updateSpaceCount(spaceCount)),
  toggleWrapMode: () => dispatch(settingsActions.toggleWrapMode()),
});

export const EditorOptions = connect(
  mapStateToProps,
  mapDispatchToProps
)(EditorOptionsComponent);
