import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import { Editor as EditorComponent } from '../components/tab-panes/editor/editor';
import { JsonActionsTypes } from '../flux/actions/ActionTypes';
import * as jsonActions from '../flux/actions/json';
import { RootStateType } from '../flux/reducers';

const mapStateToProps = (state: RootStateType) => ({
  json: state.json.text,
  parsingError: state.json.parsingError,
  isWrapModeOn: state.settings.isWrapModeOn,
});

const mapDispatchToProps = (dispatch: Dispatch<JsonActionsTypes>) => ({
  updateJson: (newValue: string) => dispatch(jsonActions.update(newValue)),
});

export const Editor = connect(
  mapStateToProps,
  mapDispatchToProps
)(EditorComponent);
