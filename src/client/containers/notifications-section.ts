import { connect } from 'react-redux';
import { Dispatch } from 'redux';

import { NotificationsSection as NotificationsSectionComponent } from '../components/notifications-section';
import { NotificationsActionTypes } from '../flux/actions/ActionTypes';
import { remove as removeNotification } from '../flux/actions/notifications';
import { RootStateType } from '../flux/reducers';
import { Notification } from '../types/Notification';

const mapStateToProps = (state: RootStateType) => ({
  notifications: state.notifications,
});

const mapDispatchToProps = (dispatch: Dispatch<NotificationsActionTypes>) => ({
  removeNotification: (notification: Notification) => dispatch(removeNotification(notification)),
});

export const NotificationsSection = connect(
  mapStateToProps,
  mapDispatchToProps
)(NotificationsSectionComponent);
