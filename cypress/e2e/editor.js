describe('editor', function() {
  it('can enter content', function() {
    const content = '["Hello Cypress"]';
    cy.visit('/')
      .getByTestId('editor-textarea')
      .clear()
      .type(content)
      .should('contain', content);
  });
});
