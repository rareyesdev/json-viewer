module.exports = function babelConfigFactory(api) {
  console.log('BABEL ENV: ', api.env());
  api.cache.using(() => process.env.NODE_ENV);
  return {
    presets: [
      [
        '@babel/preset-env',
        {
          // Enable module transformation in test only. Other environments will be handled by Webpack.
          modules: process.env.NODE_ENV === 'test' ? 'commonjs' : false,
        },
      ],
      '@babel/preset-react',
      '@emotion/babel-preset-css-prop',
      '@babel/preset-typescript',
    ],
    plugins: [
      '@babel/plugin-proposal-class-properties',
      '@babel/plugin-syntax-dynamic-import',
      [
        '@babel/plugin-transform-runtime',
        {
          corejs: 2,
          helpers: true,
          regenerator: true,
          useESModules: true,
        },
      ],
    ],
  };
};
