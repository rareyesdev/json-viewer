module.exports = {
  // Restore mocks after each test
  // https://jestjs.io/docs/en/configuration#restoremocks-boolean
  restoreMocks: true,
  // Not requiring coverage for now.
  // https://jestjs.io/docs/en/configuration#coveragethreshold-object
  coverageThreshold: undefined,
  // Only generate interesting coverages. Add json reporter if there is a way to integrate with a CI tool.
  // https://jestjs.io/docs/en/configuration#coveragereporters-array-string
  coverageReporters: ['lcov', 'text', 'text-summary'],
  // Cleanup DOM after each test
  setupFilesAfterEnv: ['react-testing-library/cleanup-after-each'],
  // Force babel to compile ES6 imports here
  transformIgnorePatterns: ['node_modules/(?!@babel/runtime-corejs2)'],
  // Configure same alias we have in Webpack config
  moduleNameMapper: {
    '^typesafe-actions$': '<rootDir>/node_modules/typesafe-actions/dist/index.esm.js',
  },
  roots: ['<rootDir>/src/', '<rootDir>/tests/'],
};
