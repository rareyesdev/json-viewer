# Json Viewer

<div align="middle">
  <a href="https://app.netlify.com/sites/json-viewer/deploys">
    <img src="https://api.netlify.com/api/v1/badges/2b6788d3-3d23-4f01-b1e9-2a1fc3602bf2/deploy-status" alt="Netlify Status">
  </a>
</div>

<div align="middle">
  <a href="http://commitizen.github.io/cz-cli/">
    <img src="https://img.shields.io/badge/commitizen-friendly-brightgreen.svg" alt="Commitizen friendly">
  </a>
  <a href="https://github.com/prettier/prettier">
    <img src="https://img.shields.io/badge/code_style-prettier-ff69b4.svg" alt="code style: prettier">
  </a>
</div>

<div align="middle">
  master
  <a href="https://gitlab.com/rareyesdev/json-viewer/commits/master">
    <img alt="Pipeline status" src="https://gitlab.com/rareyesdev/json-viewer/badges/master/pipeline.svg" />
  </a>
  <a href="https://gitlab.com/rareyesdev/json-viewer/commits/master">
    <img alt="coverage report" src="https://gitlab.com/rareyesdev/json-viewer/badges/master/coverage.svg" />
  </a>
</div>

<div align="middle">
  dev &nbsp; &nbsp;
  <a href="https://gitlab.com/rareyesdev/json-viewer/commits/develop">
    <img alt="Pipeline status" src="https://gitlab.com/rareyesdev/json-viewer/badges/develop/pipeline.svg" />
  </a>
  <a href="https://gitlab.com/rareyesdev/json-viewer/commits/develop">
    <img alt="coverage report" src="https://gitlab.com/rareyesdev/json-viewer/badges/develop/coverage.svg" />
  </a>
</div>

## Description
View and format JSON objects.

## Development
The error `error:03000086:digital envelope routines::initialization error` typically arises due to incompatibility between older code and recent versions of Node.js. This error is related to OpenSSL, a cryptographic library that Node.js uses, and it often happens because Node.js 17+ changes how it handles cryptographic functions, causing older apps that rely on legacy functions to break.

Set OpenSSL Legacy Provider
```bash
export NODE_OPTIONS=--openssl-legacy-provider
```
