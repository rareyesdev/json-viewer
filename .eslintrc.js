module.exports = {
  parser: '@typescript-eslint/parser',
  plugins: [
    '@typescript-eslint',
    'import',
    'simple-import-sort',
    'jest'
  ],
  extends: [
    'plugin:react/recommended',
    'plugin:@typescript-eslint/recommended', // Uses the recommended rules from the @typescript-eslint/eslint-plugin
    'prettier/@typescript-eslint', // Uses eslint-config-prettier to disable ESLint rules from @typescript-eslint/eslint-plugin that would conflict with prettier
    'plugin:prettier/recommended', // Enables eslint-plugin-prettier and displays prettier errors as ESLint errors. Make sure this is always the last configuration in the extends array.
    'plugin:import/typescript',
    'plugin:jest/recommended',
    'plugin:jest/style'
  ],
  parserOptions: {
    ecmaVersion: 6,
    project: './tsconfig.json',
    sourceType: 'module',
  },
  settings: {
    react: {
      version: 'detect',
    },
  },
  rules: {
    // Disable prop-types since we are using Typescript
    'react/prop-types': 'off',
    'react/jsx-sort-props': ['error', { 'callbacksLast': true }],
    // Prefer type inference over explicit return types
    '@typescript-eslint/explicit-function-return-type': 'off',
    // Allow hoisting semantic
    '@typescript-eslint/no-use-before-define': ['error', { 'functions': false }],
    // Disable default export
    'import/no-default-export': 'error',
    // Sort imports
    'simple-import-sort/sort': 'error',
  },
  overrides: [
    {
      // JS rules
      files: ['*.js'],
      rules: {
        '@typescript-eslint/no-var-requires': 'off',
      }
    },
    {
      // Test only rules
      files: ['*.test.ts', '*.test.tsx'],
      rules: {
        // Use it instead of test
        'jest/consistent-test-it': 'error',
        // Use lowercase in all test strings
        'jest/lowercase-name': 'error',
        // Forbid empty title
        'jest/no-empty-title': 'error',
        //
        'jest/no-test-callback': 'error',
        //
        'jest/no-test-return-statement': 'error',
        //
        'jest/no-truthy-falsy': 'error',
        //
        'jest/prefer-expect-assertions': 'error',
        //
        'jest/prefer-spy-on': 'error',
        //
        'jest/prefer-strict-equal': 'error',
        //
        'jest/prefer-to-be-null': 'error',
        //
        'jest/prefer-to-be-undefined': 'error',
        //
        'jest/prefer-to-contain': 'error',
        //
        'jest/prefer-to-have-length': 'error',
        //
        'jest/require-tothrow-message': 'error',
        //
        'jest/prefer-todo': 'error',
      }
    }
  ]
}
